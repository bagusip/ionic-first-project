import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirstPage } from '../first/first';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  load(){
    this.navCtrl.push(FirstPage,{
      val: 'main_to_first'
    })
  }

  clicked(){
    console.log("Button is clicked")
  }

}
