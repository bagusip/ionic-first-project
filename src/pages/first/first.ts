import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SecondPage } from '../second/second';

/**
 * Generated class for the FirstPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-first',
  templateUrl: 'first.html',
})
export class FirstPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    //console.log(navParams.get('val'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstPage');
  }

  load(){
    this.navCtrl.push(SecondPage,{
      val: 'first_to_second'
    })
  }

}
